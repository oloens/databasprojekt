<?php 
session_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title>FlowerPower</title>
<link rel="stylesheet" type="text/css" href="cssfilen.css" />
</head>

<body>

<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">

	function isEmailTaken() {
		var username = "specialname";
		var emailinput=document.getElementById("emailinput").value;
		$.post("checkuser.php", {name:username, email:emailinput},
			function(returnedData){
				var str = returnedData;
				str = str.trim();
				if (str=="1NAMEERROR&EMAILOK") {
					document.getElementById("status").innerHTML = "Account information saved successfully";
					updateInfo();
				}
				else {
				document.getElementById("status").innerHTML = "Email already taken or invalid email input!";
			
				}

				}).fail(function(){
					alert("Error");
				});

	

	}
	
	function updateInfo() {
		var emailinput=document.getElementById("emailinput").value;
		var passwordinput=document.getElementById("passwordinput").value;
		var addressinput=document.getElementById("addressinput").value;
			
		var ifpost = true;
		
		
		passwordinput = passwordinput.trim();
		addressinput = addressinput.trim();
		
		
		if (passwordinput=="")	{
			alert("You cannot enter an empty password. Enter your old password if you wish for it to remain unchanged.");
			ifpost = false;
		}
		if (addressinput=="")	{
			alert("You cannot enter an empty address. Enter your old address if you wish for it to remain unchanged.");
			ifpost = false;
		}
		
			
		if (ifpost) {
			$.post("updateacc.php", {password:passwordinput, email:emailinput, address:addressinput},
			function(returnedData){
				var returned = returnedData.trim();
				document.getElementById("status").innerHTML = returned;
				if ((returned!="Only letters and numbers allowed!")) {
					window.location = "accountpage.php";
				}

			
				}).fail(function(){
					alert("Error");
				});
		}
			
		
		
	}
</script>
<div id="wrapper">
<?php
	if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
?>
	<div id="content_area">
	<h3> Edit information</h3>
		Password: <input type="password" id="passwordinput"><br>
		Address: <input type="text" id="addressinput"><br>
		E-mail: <input type="text" id="emailinput" ><br>
 		<button onclick="isEmailTaken()">Save</button> 
		<p id="status"></p>

			
			
	</div>

	<footer>
		<p> All rights reserved.</p>
	</footer>

</div>

</body>
</html>
