<!DOCTYPE html>
<?php
	include"dbconnect.php";
	session_start();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title>FlowerPower</title>
<link rel="stylesheet" type="text/css" href="modalcss.css" />
<link rel="stylesheet" type="text/css" href="cssfilen.css" />


</head>

<body>

<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	function addToCart(id) {
		var name = id;
		var amount;
		switch (name) {
			
			case "Rosor":
			amount=document.getElementById("amount1").value;

			break;

			case "Orkideer":
			amount=document.getElementById("amount2").value;
			break;

			case "Liljor":
			amount=document.getElementById("amount3").value;
			break;

			case "Tulpaner":
			amount=document.getElementById("amount4").value;
			break;

			


			}
		$.post("addtocart.php", {prodname:id, quantity:amount},
			function(returnedData){
			alert(returnedData);
				}).fail(function(){
					alert("Error");
				});

		}
	function reviewProduct(id) {
		var prodid;
		switch (id) {
			case "review1":
			prodid=1;
			break;
			case "review2":
			prodid=2;
			break;
			case "review3":
			prodid=3;
			break;
			case "review4":
			prodid=4;
			break;
		}
		
		document.getElementById("prodReview").value=prodid;
		document.getElementById("reviewForm").submit();
		
	}
	
</script>
<div id="wrapper">
	<?php
	if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
?>

	<div class="container">
	<?php
	$sql="SELECT * FROM oloens4db.products";
	$result = $connect->query($sql);
	if ($result->num_rows==0) {
		exit("No products available");
	}
	while ($row = $result->fetch_assoc()) {
		$prodid = $row["ProductID"];
		$prodname = $row["prodname"];
		$price = $row["price"];
		$avggrade = $row["avggrading"];
		$stock = $row["stock"];
		$url = $row["imageURL"];
		
		$avggrade = floatval($avggrade);
		$avggrade = round($avggrade, 1);
		
		echo "<div class='item'><img src='$url' width='150' height='150'><br>
	<b>$prodname</b><br>Genomsnittsbetyg: {$avggrade}/5 <br>Pris: $price SEK<br>
	 Antal i lager: $stock<br>
	 <select id='amount"."$prodid'>
		  <option value='1'>1</option>
		  <option value='2'>2</option>
		  <option value='3'>3</option>
		  <option value='4'>4</option>
		  <option value='5'>5</option>
		</select>
		
		<button id='$prodname' onclick='addToCart(this.id)'>Add to cart</button>
		<button id='review"."$prodid' onclick='reviewProduct(this.id)'>Reviews</button>	</div>	";
	}
	
	
	
	
	
	
	?>

		
	</div>

	<footer>
		<p> All rights reserved.</p>
	</footer>

</div>

</body>
</html>
