<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title>FlowerPower</title>
<link rel="stylesheet" type="text/css" href="cssfilen.css" />
</head>

<body>
<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">

	function isTaken() {
		var username=document.getElementById("usernameinput").value;
		var emailinput=document.getElementById("email").value;
		if (username == "" || username.length < 5 ) {
 			document.getElementById("test").innerHTML = "Username too short";
		}
		else {
		$.post("checkuser.php", {name:username, email:emailinput},
			function(returnedData){
				var status;
				var returned = returnedData.trim();
				var displaystatus = true;
				if (returned=="1NAMEOK&EMAILOK") {
					displaystatus=false;
					createUser();
					
					}
				else if (returned=="1NAMEOK&EMAILERROR") {
					status="That email is already in use!";
					}
				else if (returned=="1NAMEERROR&EMAILOK") {
					status="That username is already in use!";
					}
				else if (returned=="1NAMEERROR&EMAILERROR") {
					status="That username and email is already in use!";
					}
				
				else {
					status="Invalid email format! or possibly unknown error";

					}
				if (displaystatus) {
					document.getElementById("test").innerHTML=status;
					}
				


				}).fail(function(){
					alert("Error");
				});


				
		 }
	

}

function createUser() {

	var usernameinput=document.getElementById("usernameinput").value;
	var passwordinput=document.getElementById("passwordinput").value;
	var firstnameinput=document.getElementById("firstname").value;
	var lastnameinput=document.getElementById("lastname").value;
	var emailinput=document.getElementById("email").value;
	var addressinput=document.getElementById("address").value;
	

	$.post("createuser.php", {name:usernameinput, email:emailinput, password:passwordinput, firstname:firstnameinput, 						lastname:lastnameinput, address:addressinput},
		function(returnedData){
			document.getElementById("test").innerHTML=returnedData;
			}).fail(function(){
				alert("Error");
			});

	
}


</script>

<div id="wrapper">
<?php
	if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
?>
	<div id="content_area">
		Username: <input id="usernameinput" type="text" ><br>
		Password: <input type="password" id="passwordinput"><br>
		First name: <input id="firstname" type="text" ><br>
		Last name: <input type="text" id="lastname" ><br>
		Address: <input type="text" id="address"><br>
		E-mail: <input type="text" id="email" ><br>
 		<button onclick="isTaken()">Create Account</button> 
		<p id="test"></p>

			
			
	</div>

	<footer>
		<p> All rights reserved.</p>
	</footer>

</div>

</body>
</html>
