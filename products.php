<!DOCTYPE html>
<?php
	include"dbconnect.php";
	session_start();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title>FlowerPower</title>
<link rel="stylesheet" type="text/css" href="modalcss.css" />
<link rel="stylesheet" type="text/css" href="cssfilen.css" />


</head>

<body>

<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	function addToCart(id) {
		var prodid = id;
		prodid = prodid.replace(/[^0-9]/g, '');
		
		
		var amountid = "amount".concat(prodid);
		var amount = document.getElementById(amountid).value;
		
		$.post("addtocart.php", {productid:prodid, quantity:amount},
			function(returnedData){
			alert(returnedData);
				}).fail(function(){
					alert("Error");
				});

		}
	function reviewProduct(id) {
		var prodid = id;
		prodid = prodid.replace(/[^0-9]/g, '');
		
		document.getElementById("prodReview").value=prodid;
		document.getElementById("reviewForm").submit();
		
	}
	
	function editProduct(id) {
		var prodid = id;
		prodid = prodid.replace(/[^0-9]/g, '');
		
		document.getElementById("prodedit").value=prodid;
		document.getElementById("editForm").submit();
		
	}
	
</script>
<div id="wrapper">
	<?php
	if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
?>
	
	<div class="container">
	<form method='post' id='reviewForm' action='reviewPage.php'>
		<input id='prodReview' type='hidden' name='prodid' >
	</form>
	<?php
	
	if (isset($_SESSION["admin"])) {
		echo "<form method='post' id='editForm' action='editProduct.php'>
		<input id='prodedit' type='hidden' name='prodid' >
		</form>";
	} 
	?>
	
	<?php
	$sql="SELECT * FROM oloens4db.products";
	$result = $connect->query($sql);
	if ($result->num_rows==0) {
		exit("No products available");
	}
	
	while ($row = $result->fetch_assoc()) {
		$display = $row["display"];
		if ($display==0) {
			continue;
		}
		$prodid = $row["ProductID"];
		$prodname = $row["prodname"];
		$price = $row["price"];
		$avggrade = $row["avggrading"];
		$stock = $row["stock"];
		$url = $row["imageURL"];
		
		$avggrade = floatval($avggrade);
		$avggrade = round($avggrade, 1);
		if (isset($_SESSION["admin"])) {
			echo "<div class='item'><img src='$url' width='150' height='150'><br>
	<b>$prodname</b><br>Genomsnittsbetyg: {$avggrade}/5 <br>Pris: $price SEK<br>
	 Antal i lager: $stock<br> <button id='editProduct"."$prodid' onclick='editProduct(this.id)'>
	 Edit Product</button> <button id='review"."$prodid' onclick='reviewProduct(this.id)'>Reviews</button></div>";
		}
		else {
			echo "<div class='item'><img src='$url' width='150' height='150'><br>
	<b>$prodname</b><br>Genomsnittsbetyg: {$avggrade}/5 <br>Pris: $price SEK<br>
	 Antal i lager: $stock<br>
	 <select id='amount"."$prodid'>
		  <option value='1'>1</option>
		  <option value='2'>2</option>
		  <option value='3'>3</option>
		  <option value='4'>4</option>
		  <option value='5'>5</option>
		</select>
		
		<button id='addtocart"."$prodid' onclick='addToCart(this.id)'>Add to cart</button>
		<button id='review"."$prodid' onclick='reviewProduct(this.id)'>Reviews</button>	</div>	";
		}
		
	}
	
	
	
	
	
	
	?>

		
	</div>

	<footer>
	<?php
	if (isset($_SESSION["admin"])) {
		echo "<form action='newproduct.php'><button type='submit' style='float:right'>
		Add new product</button></form>";
	} ?>
		<p> All rights reserved.</p>
	</footer>

</div>

</body>
</html>
