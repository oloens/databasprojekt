<!DOCTYPE html>
<?php
	include"dbconnect.php";
	session_start();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title>FlowerPower</title>
<link rel="stylesheet" type="text/css" href="modalcss.css" />
<link rel="stylesheet" type="text/css" href="cssfilen.css" />


</head>

<body>

<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	
	
	function editProduct(id) {
		
		var prodid = id;
		prodid = prodid.replace(/[^0-9]/g, '');
		
		document.getElementById("prodedit").value=prodid;
		document.getElementById("editForm").submit();
	
	}

</script>
<div id="wrapper">
	<?php
	if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
?>
	<?php
	$sql="SELECT * FROM oloens4db.products WHERE display=0";
	$result = $connect->query($sql);
	if ($result->num_rows==0) {
		echo "<div id='content_area'> No products available!";
	}
	else {
		echo "<div class='container'>";
	}

	if (isset($_SESSION["admin"])) {
		echo "<form method='post' id='editForm' action='editProduct.php'>
		<input id='prodedit' type='hidden' name='prodid' >
		</form>";
	} 
	?>
	
	<?php
	
	
	while ($row = $result->fetch_assoc()) {
		$display = $row["display"];
		if ($display==1) {
			continue;
		}
		$prodid = $row["ProductID"];
		$prodname = $row["prodname"];
		$price = $row["price"];
		$avggrade = $row["avggrading"];
		$stock = $row["stock"];
		$url = $row["imageURL"];
		
		$avggrade = floatval($avggrade);
		$avggrade = round($avggrade, 1);
		if (isset($_SESSION["admin"])) {
			echo "<div class='item'><img src='$url' width='150' height='150'><br>
	<b>$prodname</b><br>Genomsnittsbetyg: {$avggrade}/5 <br>Pris: $price SEK<br>
	 Antal i lager: $stock<br> <button id='editProduct"."$prodid' onclick='editProduct(this.id)'>
	 Restore Product</button></div>";
		}
		
	}
	
	
	
	
	
	
	?>

		
	</div>

	<footer>
	
		<p> All rights reserved.</p>
	</footer>

</div>

</body>
</html>
