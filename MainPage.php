<!DOCTYPE html>
<?php
	include"dbconnect.php";

?>
<?php
session_start();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title> FlowerPower</title>
<link rel="stylesheet" type="text/css" href="cssfilen.css" />
</head>

<body>

<div id="wrapper">
	<?php
		if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
		
	?>
	
	<div id="content_area">

		<p> Välkommen till FlowerPower, den absolut bästa butiken för att handla blommor online! 
		Kom igång direkt genom att göra en användare och logga in, och beställ sen alla produkter du någonsin
		hade kunnat önska dig!
		
		Mvh Bosse och personal</p>
	</div>

	<footer>
		<p> All rights reserved.</p>
	</footer>

</div>


</body>
