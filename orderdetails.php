<!DOCTYPE html>
<?php
	include"dbconnect.php";
	session_start();

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
<title> FlowerPower</title>
<link rel="stylesheet" type="text/css" href="cssfilen.css" />
</head>

<body>
<script src="jquery-3.1.1.min.js"></script>
<script type="text/javascript">

	function shipProduct(thisorderid) {
		$.post("shipProduct.php", {OrderID:thisorderid, ifcancel:"false"},
		function(returnedData){
			location.reload();
			}).fail(function(){
				alert("Error");
			});
	}

	function cancelOrder(thisorderid) {
		$.post("shipProduct.php", {OrderID:thisorderid, ifcancel:"true"},
		function(returnedData){
			location.reload();
			}).fail(function(){
				alert("Error");
			});
	}

</script>
<div id="wrapper">
	<?php
		if (isset($_SESSION["login"])) {
			include "bannernav_loggedin.php";
		}
		else {
			include "bannernav.php";
		}
		
	?>
	
	<div id="content_area">
		<?php
			if (!isset($_SESSION["login"])){
				exit("not logged in should never be seen");
			}
			$username = $_POST["username"];
			$orderID = intval($_POST['orderid']);
			$sql = "SELECT * FROM oloens4db.orders JOIN oloens4db.orderdetails
			ON oloens4db.orders.OrderID=oloens4db.orderdetails.OrderID WHERE oloens4db.orders.OrderID=$orderID";
			$result = $connect->query($sql);


			
		
			
			
			$displayed = 0;
			
			echo " <div class='item2'><h3>Order details for order #$orderID :</h3> <br>";
			$total = 0;
			while ($row=$result->fetch_assoc()) {
				
				if ($displayed==0) {
					$orderdate = $row["OrderDate"];
					$shipdate = $row["ShipDate"];
					$orderstatus = $row["OrderStatus"];
					$userid = $row["UserID"];
					if ($shipdate===NULL){
						$shipdate="Not yet shipped";
					}
					if ($orderstatus===NULL){
						$orderstatus="Processing order";
					}
					
					echo "Order placed: $orderdate <br> Shipped on: $shipdate <br>
					Order status: $orderstatus <br><br>";
					$displayed = 1;
					
					
				}
				
				$prodid = $row["ProductID"];
				
				$sql2 = "SELECT prodname FROM oloens4db.products WHERE ProductID=$prodid";
				$result2 = $connect->query($sql2);
				$row2 = $result2->fetch_assoc();
			
				
				$prodname = $row2["prodname"];
				$quantity = $row["Quantity"];
				$price = $row["price"];
				$prodtotal = $price*$quantity;
				$total = $total+$prodtotal;
				echo "Product: $prodname x$quantity = $prodtotal SEK<br>";
				
				
				
				
			}
			echo "<br> For a total of $total SEK.";
			if (isset($_SESSION["admin"])) {
				if (!($orderstatus=="Shipped" || $orderstatus=="Cancelled")){
					echo "<br><br><button onclick='shipProduct($orderID)'>Ship Order</button>
						<button onclick='cancelOrder($orderID)'>Cancel Order</button></div>";
				}
				else {
					echo "</div>";
				}
				
				
				
				$sql = "SELECT * FROM oloens4db.users WHERE UserID=$userid";
				$result = $connect->query($sql);
				$row = $result->fetch_assoc();
				
				$username = $row["username"];
				$userid = $row["UserID"];
				$firstname = $row["firstname"];
				$lastname = $row["lastname"];
				$address = $row["address"];
				$email = $row["email"];
				
				echo "<div class='item2' style='float:right'> <h3> Order made by user $username :</h3>
					User ID: $userid <br> First name: $firstname <br>
					Last name: $lastname <br> Address: $address <br>
					E-mail: $email <br></div>";
					
				
				
			}
			else {
				
		
		echo "<form action='orderpage.php' method='post'>
			<input type='hidden' name='username' value='$username'>
			<button type='submit' id='orderButton'>Back</button></form>";
			if (!($orderstatus=="Shipped" || $orderstatus=="Cancelled")) {
				echo "<button onclick='cancelOrder($orderID)'>Cancel order</button>";
				}
			 echo "</div>";
			}
		
			
		?>

		


	
				
	</div>


	<footer>
		<p> All rights reserved.</p>
	</footer>
</div>


</body>
